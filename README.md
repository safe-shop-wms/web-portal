# Web Portal

The Web Portal for the Safe Shop Management System.

## Getting Started

### Install Dependencies

The following must be installed before working with the app.

* [Node 12](https://nodejs.org/dist/latest-v12.x/)

### Setup Backend Services

The web portal relies on a serverless backend. See the [Infrastructure Setup](https://gitlab.com/safe-shop-wms/product-backlog/-/wikis/Setup-Guide/Infrastructure-Setup) wiki for instructions to setup the backend services and update the web portal configuration files.

## Run It

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Test It

This app uses the Jest testing frameork. Tests are placed in the same directory as the component which they test with the filename, 'ComponentName.test.js'. The 'mockRequests' directory currently contains methods to use as mock responses for network requests. As the testing is further developed, expect the organization to change slightly.

### Writing Tests

The current focus is on writing basic tests for each component. The following references cover some common tools used in the tests:

* [Jest Mock Functions](https://jestjs.io/docs/en/mock-functions.html)
* [Testing Library Queries](https://testing-library.com/docs/queries/about)

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Build and Deploy It

The build and deploy scripts should be run with the appropriate environment identifier so that the correct environment variables are utilized to connect to the desired backend services. The following environments are defined:

* test -> `npm run <COMMAND>:test`
* prod -> `npm run <COMMAND>:prod`

### `npm run build:<ENV>`

Builds the app for the specified environment to the `build` folder. \
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run deploy:<ENV>`

`npm run build:<ENV>` must be run prior to running this script. Deploys the app from the `build` folder to the Amazon S3 bucket specified in the environment file (.env).

The test and prod deployments are run by the pipeline on pushes to `develop` and `master` respectively. If these commands are run locally, AWS credentials for a CLI user with the appropriate permissions must be available in the local environment.

## References

### Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

#### Script: `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
