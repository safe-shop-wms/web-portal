import React from 'react'
import Typography from '@material-ui/core/Typography'

// Screen to display when route is not found. 
export default function NotFound() {
    return (
        <Typography>
            Whoops! That page isn't found.
        </Typography>
    );
}