// Route Name Constants

const ROUTE_NAME  = {
    HOME: "/",
    USERS: "/users",
    USER_EDIT: "/users/edit",
    TOOLS: "/tools",
    TOOL_EDIT: "/tools/edit",
    TRAINING: "/training",
    TRAINING_EDIT: "/training/edit",
    LOGIN: "/login",
    ONBOARDING: "/onboarding"
}

export default ROUTE_NAME;