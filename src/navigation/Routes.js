import React from "react";
import LoginPage from '../pages/LoginPage';
import OnboardingPage from '../pages/OnboardingPage';
import UsersPage from '../pages/UsersPage';
import UserEditPage from '../pages/UserEditPage';
import ToolsPage from '../pages/ToolsPage';
import ToolEditPage from '../pages/ToolEditPage';
import TrainingPage from '../pages/TrainingPage';
import TrainingEditPage from '../pages/TrainingEditPage';
import NotFound from './NotFound';
import { Redirect, Route, Switch } from "react-router-dom";
import ROUTE_NAME from './CONSTANTS';

// Routes for the router. Unauthenticated users are redirected to the login page automatically.
export default function Routes({props, setIsAuthenticated, isAuthenticated, setUserData, userData}) { 
   return (<Switch>
   <Route exact path={ROUTE_NAME.HOME} render={() => isAuthenticated ? <Redirect to={ROUTE_NAME.USERS}/> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.USERS} render={() => isAuthenticated ? <UsersPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.USER_EDIT} render={() => isAuthenticated ? <UserEditPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.TOOLS} render={() => isAuthenticated ? <ToolsPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.TOOL_EDIT} render={() => isAuthenticated ? <ToolEditPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.TRAINING} render={() => isAuthenticated ? <TrainingPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.TRAINING_EDIT} render={() => isAuthenticated ? <TrainingEditPage props={props} /> : <Redirect to={ROUTE_NAME.LOGIN}/>}/>
   <Route exact path={ROUTE_NAME.LOGIN} render={() => <LoginPage props={props} setIsAuthenticated={setIsAuthenticated} setUserData={setUserData} />}/>
   <Route exact path={ROUTE_NAME.ONBOARDING} render={() => <OnboardingPage props={props} setIsAuthenticated={setIsAuthenticated} userData={userData}/>}/>
   <Route component={NotFound} />
</Switch>);
}