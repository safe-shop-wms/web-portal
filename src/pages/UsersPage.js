import React, { useEffect, useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import ConfirmActionDialog from '../components/ConfirmActionDialog';

import { getTools } from '../utils/ToolApi';
import { getTraining } from '../utils/TrainingApi';
import { getUsers, deleteUser } from '../utils/UserApi';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { IconButton, Typography } from '@material-ui/core';
import { EDIT_TYPE } from '../utils/PageTypeConstants';
import ROUTE_NAME from '../navigation/CONSTANTS';
import { sortById } from '../utils/DataUtils';

// Page to list users
export default function UsersPage({ props }) {

  const [isConfirmationDialogOpen, setConfirmationDialogOpen] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState("-1");

  // Track state of data loaded
  const [userDataState, setUserDataState] = useState([{
    userError: null,
    userIsLoaded: false,
    userItems: []
  }]);
  const [toolDataState, setToolDataState] = useState([{
    toolError: null,
    toolIsLoaded: false,
    toolItems: []
  }]);
  const [trainingDataState, setTrainingDataState] = useState([{
    trainingError: null,
    trainingIsLoaded: false,
    trainingItems: []
  }]);
  const [shouldRefreshUserData, setShouldRefreshUserData] = useState(false);
  const [shouldRefreshToolData, setShouldRefreshToolData] = useState(false);
  const [shouldRefreshTrainingData, setShouldRefreshTrainingData] = useState(false);

  useEffect(() => {
    let isMounted = true;
    async function loadUserData() {
      try {
        let result = await getUsers()
        if (isMounted) {
          let items = result.data.Items;
          let sortedItems = items.sort(sortById());
          setUserDataState({
            userIsLoaded: true,
            userItems: sortedItems
          });
        }

      } catch (error) {
        if (isMounted) {
          setUserDataState({
            userIsLoaded: true,
            userError: error
          });
        }

        alert(error);
      }
      setShouldRefreshUserData(false);
    }

    async function loadToolData() {
      try {
        let result = await getTools()
        if (isMounted) {
          let items = result.data.Items;
          let sortedItems = items.sort(sortById());
          setToolDataState({
            toolIsLoaded: true,
            toolItems: sortedItems
          });
        }
      } catch (error) {
        if (isMounted) {
          setToolDataState({
            toolIsLoaded: true,
            toolError: error
          });
        }
        alert(error);
      }
      setShouldRefreshToolData(false);
    }

    async function loadTrainingData() {
      try {
        let result = await getTraining()
        if (isMounted) {
          let items = result.data.Items;
          let sortedItems = items.sort(sortById());
          setTrainingDataState({
            trainingIsLoaded: true,
            trainingItems: sortedItems
          });
        }
      } catch (error) {
        if (isMounted) {
          setTrainingDataState({
            trainingIsLoaded: true,
            trainingError: error
          });
        }
        alert(error);
      }
      setShouldRefreshTrainingData(false);
    }

    loadUserData();
    loadToolData();
    loadTrainingData();
    return (() => { isMounted = false });
  }, [shouldRefreshUserData, shouldRefreshToolData, shouldRefreshTrainingData]);
  // Prevent API call from being made except at first run and when triggered by user action.

  async function onAddUser() {
    props.history.push({
      pathname: ROUTE_NAME.USER_EDIT,
      state: {
        "editType": EDIT_TYPE.NEW,
        "toolData": toolDataState.toolItems,
        "trainingData": trainingDataState.trainingItems
      }
    });
  }

  async function onEditUser(userData) {
    props.history.push({
      pathname: ROUTE_NAME.USER_EDIT,
      state: {
        "editType": EDIT_TYPE.UPDATE,
        "userData": userData,
        "toolData": toolDataState.toolItems,
        "trainingData": trainingDataState.trainingItems
      }
    });
  }

  function onDeleteUser(userId) {
    setSelectedUserId(userId);
    setConfirmationDialogOpen(true);
  }

  async function onConfirmDelete() {
    setConfirmationDialogOpen(false);
    try {
      await deleteUser(selectedUserId);
      setShouldRefreshUserData(true);
    } catch (error) {
      alert(`There was an error deleting the user. ${error}`);
    }
  }

  function onCancelDelete() {
    setConfirmationDialogOpen(false);
  }

  function getToolNames(toolIds) {
    if (toolIds) {
      var toolNames = "";
      let toolArray = toolIds.NS;
      for (var toolId of toolArray) {
        for (var toolData of toolDataState.toolItems) {
          if (toolData.id.N === toolId) {
            if (toolNames !== "") toolNames += ', ';
            toolNames = toolNames + toolData.name.S;
            break;
          }
        }
      }
      return toolNames;
    }
    return "";
  }

  return (
    userDataState.error || toolDataState.error || trainingDataState.error ?
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
            An error occurred accessing the API. Please try again later.
        </Alert>
      : (!userDataState.userIsLoaded || !toolDataState.toolIsLoaded || !trainingDataState.trainingIsLoaded) ?
        <CircularProgress data-testid='circular-progress' />
        :
        <Container>
          <div style={{ width: '100%' }}>
            <Box display="flex" mb={2}>
              <Box flexGrow={1} >
                <Typography variant="h5">User Management</Typography>
              </Box>
              <Box>
                <Button align="justify-right" variant="contained" color="primary" onClick={() => onAddUser()} >Add User</Button>
              </Box>
            </Box>
          </div>

          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell align="center">First Name</TableCell>
                  <TableCell align="center">Last Name</TableCell>
                  <TableCell align="center">Card ID</TableCell>
                  <TableCell align="center">Authorized Tools</TableCell>
                  <TableCell align="center"></TableCell>
                  <TableCell align="center"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userDataState.userItems.map((item) => (
                  <TableRow key={item.id.N}>
                    <TableCell component="th" scope="row">
                      {item.id.N}
                    </TableCell>
                    <TableCell align="center">{item.first_name ? item.first_name.S : ""}</TableCell>
                    <TableCell align="center">{item.last_name ? item.last_name.S : ""}</TableCell>
                    <TableCell align="center">{item.card_id ? item.card_id.S : ""}</TableCell>
                    <TableCell align="center">
                      {getToolNames(item.authorized_tools)}
                    </TableCell>
                    <TableCell align="center"><IconButton onClick={() => onEditUser(item)}><EditIcon /></IconButton></TableCell>
                    <TableCell align="center"><IconButton onClick={() => onDeleteUser(item.id.N)}><DeleteIcon /></IconButton></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <ConfirmActionDialog actionDescription="Delete user" itemId={selectedUserId} onCancel={onCancelDelete} onConfirm={onConfirmDelete} open={isConfirmationDialogOpen} />
        </Container>
  );
}