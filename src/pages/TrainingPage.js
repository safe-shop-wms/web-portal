import React, { useEffect, useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getTraining } from '../utils/TrainingApi';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, Typography } from '@material-ui/core';
import { EDIT_TYPE } from '../utils/PageTypeConstants';
import ROUTE_NAME from '../navigation/CONSTANTS';
import { sortById } from '../utils/DataUtils';

// Page to list training
export default function TrainingPage({ props }) {

  const [dataState, setDataState] = useState([{
    error: null,
    isLoaded: false,
    items: []
  }]);
  const [shouldRefreshData, setShouldRefreshData] = useState(false);


  useEffect(() => {
    let isMounted = true;
    async function loadTrainingData() {
      try {
        let result = await getTraining()
        if (isMounted) {
          let items = result.data.Items;
          let sortedItems = items.sort(sortById());
          setDataState({
            isLoaded: true,
            items: sortedItems,
          });
        }
      } catch (error) {
        if (isMounted) {
          setDataState({
            isLoaded: true,
            error: error
          });
        }

        alert(error);
      }
      setShouldRefreshData(false);
    }
    loadTrainingData();
    return (() => { isMounted = false });
  }, [shouldRefreshData]);
  // Prevent API call from being made except at first run and when triggered by user action.

  async function onAddTraining() {
    props.history.push({
      pathname: ROUTE_NAME.TRAINING_EDIT,
      state: {
        "editType": EDIT_TYPE.NEW
      }
    });
  }

  async function onEditTraining(trainingData) {
    props.history.push({
      pathname: ROUTE_NAME.TRAINING_EDIT,
      state: {
        "editType": EDIT_TYPE.UPDATE,
        "trainingData": trainingData
      }
    });
  }

  return (
    dataState.error ?
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
            An error occurred accessing the API. Please try again later.
        </Alert>
      : (!dataState.isLoaded) ?
        <CircularProgress data-testid='circular-progress' />
        :
        <Container>
          <div style={{ width: '100%' }}>
            <Box display="flex" mb={2}>
              <Box flexGrow={1} >
                <Typography variant="h5">Training Management</Typography>
              </Box>
              <Box>
                <Button align="justify-right" variant="contained" color="primary" onClick={() => onAddTraining()} >Add Training</Button>
              </Box>
            </Box>
          </div>

          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell align="center">Name</TableCell>
                  <TableCell align="center"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataState.items.map((item) => (
                  <TableRow key={item.id.N}>
                    <TableCell component="th" scope="row">
                      {item.id.N}
                    </TableCell>
                    <TableCell align="center">{item.name ? item.name.S : ""}</TableCell>
                    <TableCell align="center"><IconButton onClick={() => onEditTraining(item)}><EditIcon /></IconButton></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
  );
}