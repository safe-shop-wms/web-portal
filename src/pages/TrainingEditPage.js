import React, { useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { addTraining, updateTraining } from '../utils/TrainingApi';
import ROUTE_NAME from '../navigation/CONSTANTS';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { EDIT_TYPE } from '../utils/PageTypeConstants';

// Edit page to add or update a training.

// Page styling
const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function TrainingEditPage({ props }) {
  const classes = useStyles();
  const newTraining = props.location.state.editType === EDIT_TYPE.NEW ? true : false;
  const training = props.location.state.trainingData;

  // Set initial values using existing training data if not a new training.
  const [id, setId] = useState(newTraining ? "" : training.id.N);
  const [name, setName] = useState(newTraining || !training.name ? "" : training.name.S);

  // Track whether updatable values were updated
  const [nameUpdated, setNameUpdated] = useState(false);

  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChangeById = (event) => {
    switch (event.target.id) {
      case "id":
        setId(event.target.value);
        break;
      case "name":
        setName(event.target.value);
        if (!nameUpdated) setNameUpdated(true);
        break;
      default:
      // Unreachable. Update failed.
    }
  };

  async function onCancel() {
    props.history.replace(ROUTE_NAME.TRAINING);
  }

  async function onSubmit() {
    setIsSubmitting(true);
    try {
      if (newTraining) {
        if (id === "" || name === "") {
          setIsSubmitting(false);
          alert("ID and Name are all required fields. Complete all fields before submitting.");
        } else {
          await addTraining(id, name);
          props.history.replace(ROUTE_NAME.TRAINING);
        }
      } else {
        if (!nameUpdated) {
          setIsSubmitting(false);
          alert("No values updated.");
        } else {
          let updatedInfo = {};
          if (nameUpdated) {
            updatedInfo["name"] = name;
          }
          await updateTraining(id, updatedInfo);
          props.history.replace(ROUTE_NAME.TRAINING);
        }
      }
    } catch (error) {
      alert(`Error occured when submitting the data. ${error}`);
    }
  }

  return (
    <Container>
      <div style={{ width: '100%' }}>
        <Box display="flex" mb={2}>
          <Box flexGrow={1} >
            <Typography variant="h5">{newTraining ? "Add Training" : "Edit Training"}</Typography>
          </Box>
          <Box>
            <Button disabled={isSubmitting} variant="contained" color="primary" onClick={() => onSubmit()} >Submit</Button>
          </Box>
          <Box ml={2}>
            <Button disabled={isSubmitting} variant="contained" onClick={() => onCancel()} >Cancel</Button>
          </Box>
        </Box>
      </div>

      <form className={classes.root} autoComplete="off">
        <div>
          <TextField
            required
            id="id"
            label="ID"
            type="number"
            InputProps={{
              readOnly: newTraining ? false : true,
            }}
            variant="outlined"
            value={id}
            onChange={handleChangeById}
          />
          <TextField
            required
            id="name"
            label="Name"
            variant="outlined"
            value={name}
            onChange={handleChangeById}
          />
        </div>
      </form>
    </Container>
  );
}
