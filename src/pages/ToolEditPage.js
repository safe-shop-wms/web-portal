import React, { useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { TOOL_TYPE, TOOL_RESTRICTION, addTool, updateTool } from '../utils/ToolApi';
import ROUTE_NAME from '../navigation/CONSTANTS';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { MenuItem } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { EDIT_TYPE } from '../utils/PageTypeConstants';

// Edit page to add or update a tool.

// Page styling
const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function ToolEditPage({ props }) {
  const classes = useStyles();
  const newTool = props.location.state.editType === EDIT_TYPE.NEW ? true : false;
  const tool = props.location.state.toolData;

  // Set initial values using existing tool data if not a new tool.
  const [id, setId] = useState(newTool ? "" : tool.id.N);
  const [name, setName] = useState(newTool || !tool.name ? "" : tool.name.S);
  const [type, setType] = useState(newTool || !tool.type ? "" : tool.type.S);
  const [restriction, setRestriction] = useState(newTool || !tool.restriction ? "" : tool.restriction.S);
  const [enabled, setEnabled] = useState(newTool ? false : tool.enabled.BOOL);

  // Track whether updatable values were updated
  const [nameUpdated, setNameUpdated] = useState(false);
  const [typeUpdated, setTypeUpdated] = useState(false);
  const [restrictionUpdated, setRestrictionUpdated] = useState(false);
  const [enabledUpdated, setEnabledUpdated] = useState(false);

  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChangeById = (event) => {
    switch (event.target.id) {
      case "id":
        setId(event.target.value);
        break;
      case "name":
        setName(event.target.value);
        if (!nameUpdated) setNameUpdated(true);
        break;
      case "type":
        setType(event.target.value);
        if (!typeUpdated) setTypeUpdated(true);
        break;
      case "restriction":
        setRestriction(event.target.value);
        if (!restrictionUpdated) setRestrictionUpdated(true);
        break;
      case "enabled":
        setEnabled(event.target.checked);
        if (!enabledUpdated) setEnabledUpdated(true);
        break;
      default:
      // Unreachable. Update failed.
    }
  };

  const handleChangeByName = (event) => {
    switch (event.target.name) {
      case "type":
        setType(event.target.value);
        if (!typeUpdated) setTypeUpdated(true);
        break;
      case "restriction":
        setRestriction(event.target.value);
        if (!restrictionUpdated) setRestrictionUpdated(true);
        break;
      default:
      // Unreachable. Update failed.
    }
  };

  async function onCancel() {
    props.history.replace(ROUTE_NAME.TOOLS);
  }

  async function onSubmit() {
    setIsSubmitting(true);
    try {
      if (newTool) {
        if (id === "" || name === "" || type === "" || restriction === "") {
          setIsSubmitting(false);
          alert("ID, Name, Type, and Restriction are all required fields. Complete all fields before submitting.");
        } else {
          await addTool(id, name, type, restriction, enabled);
          props.history.replace(ROUTE_NAME.TOOLS);
        }
      } else {
        if (!nameUpdated && !typeUpdated && !restrictionUpdated && !enabledUpdated) {
          setIsSubmitting(false);
          alert("No values updated.");
        } else {
          let updatedInfo = {};
          if (nameUpdated) {
            updatedInfo["name"] = name;
          }
          if (typeUpdated) {
            updatedInfo["type"] = type;
          }
          if (restrictionUpdated) {
            updatedInfo["restriction"] = restriction;
          }
          if (enabledUpdated) {
            updatedInfo["enabled"] = enabled;
          }
          await updateTool(id, updatedInfo);
          props.history.replace(ROUTE_NAME.TOOLS);
        }
      }
    } catch (error) {
      alert(`Error occured when submitting the data. ${error}`);
    }
  }

  return (
    <Container>
      <div style={{ width: '100%' }}>
        <Box display="flex" mb={2}>
          <Box flexGrow={1} >
            <Typography variant="h5">{newTool ? "Add Tool" : "Edit Tool"}</Typography>
          </Box>
          <Box>
            <Button disabled={isSubmitting} variant="contained" color="primary" onClick={() => onSubmit()} >Submit</Button>
          </Box>
          <Box ml={2}>
            <Button disabled={isSubmitting} variant="contained" onClick={() => onCancel()} >Cancel</Button>
          </Box>
        </Box>
      </div>

      <form className={classes.root} autoComplete="off">
        <div>
          <TextField
            required
            id="id"
            label="ID"
            type="number"
            InputProps={{
              readOnly: newTool ? false : true,
            }}
            variant="outlined"
            value={id}
            onChange={handleChangeById}
          />
          <TextField
            required
            id="name"
            label="Name"
            variant="outlined"
            value={name}
            onChange={handleChangeById}
          />
          <TextField
            required
            select
            name="type"
            label="Type"
            variant="outlined"
            value={type}
            onChange={handleChangeByName}
          >
            <MenuItem key={TOOL_TYPE.HANDHELD} value={TOOL_TYPE.HANDHELD}>{TOOL_TYPE.HANDHELD}</MenuItem>
            <MenuItem key={TOOL_TYPE.STATIONARY} value={TOOL_TYPE.STATIONARY}>{TOOL_TYPE.STATIONARY}</MenuItem>
          </TextField>
          <TextField
            required
            select
            name="restriction"
            label="Restriction"
            variant="outlined"
            value={restriction}
            onChange={handleChangeByName}
          >
            <MenuItem key={TOOL_RESTRICTION.UNRESTRICTED} value={TOOL_RESTRICTION.UNRESTRICTED}>{TOOL_RESTRICTION.UNRESTRICTED}</MenuItem>
            <MenuItem key={TOOL_RESTRICTION.FIFTEEN_PLUS} value={TOOL_RESTRICTION.FIFTEEN_PLUS}>{TOOL_RESTRICTION.FIFTEEN_PLUS}</MenuItem>
            <MenuItem key={TOOL_RESTRICTION.ADULT_ONLY} value={TOOL_RESTRICTION.ADULT_ONLY}>{TOOL_RESTRICTION.ADULT_ONLY}</MenuItem>
          </TextField>
          <FormControlLabel
            control={
              <Checkbox
                id="enabled"
                checked={enabled}
                onChange={handleChangeById}
                name="enabled"
                color="primary"
              />
            }
            label="Enabled"
          />
        </div>
      </form>
    </Container>
  );
}
