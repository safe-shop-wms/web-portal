import React from 'react';
import { render, screen, waitForElementToBeRemoved } from '@testing-library/react';
import axios from 'axios';
import UserTable from '../components/UserTable';
import { getUsersResponse } from "../mockRequests/users"

jest.mock('axios');

// Needs update
test('Progress indicators shows, followed by data from API', async () => {
  axios.get.mockResolvedValue(getUsersResponse);
  render(<UserTable />);

  const circularProgress = screen.getByTestId(/circular-progress/i);
  expect(circularProgress).toBeInTheDocument();

  await waitForElementToBeRemoved(() => screen.getByTestId(/circular-progress/i));

  expect(axios.get).toHaveBeenCalledTimes(1);
  
  for(var i = 0; i < 10; i++) {
    expect(screen.getByText(`First_${i}`)).toBeInTheDocument();
  }
});

test('Progress indicators shows, followed by error from API', async () => {
  axios.get.mockImplementationOnce(() =>
  Promise.reject(new Error('Network Error')));
  render(<UserTable />);

  const circularProgress = screen.getByTestId(/circular-progress/i);
  expect(circularProgress).toBeInTheDocument();

  await waitForElementToBeRemoved(() => screen.getByTestId(/circular-progress/i));

  expect(axios.get).toHaveBeenCalledTimes(1);
  
  expect(screen.getByText("An error occurred accessing the API. Please try again later.")).toBeInTheDocument();
});


