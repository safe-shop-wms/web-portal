import React, { useEffect, useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getTools } from '../utils/ToolApi';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, Typography } from '@material-ui/core';
import { EDIT_TYPE } from '../utils/PageTypeConstants';
import ROUTE_NAME from '../navigation/CONSTANTS';
import Checkbox from '@material-ui/core/Checkbox';
import { sortById } from '../utils/DataUtils';

// Page to list tools
export default function ToolsPage({ props }) {

  const [dataState, setDataState] = useState([{
    error: null,
    isLoaded: false,
    items: []
  }]);
  const [shouldRefreshData, setShouldRefreshData] = useState(false);


  useEffect(() => {
    let isMounted = true;
    async function loadToolData() {
      try {
        let result = await getTools()
        if (isMounted) {
          let items = result.data.Items;
          let sortedItems = items.sort(sortById());
          setDataState({
            isLoaded: true,
            items: sortedItems,
          });
        }
      } catch (error) {
        if (isMounted) {
          setDataState({
            isLoaded: true,
            error: error
          });
        }

        alert(error);
      }
      setShouldRefreshData(false);
    }
    loadToolData();
    return (() => { isMounted = false });
  }, [shouldRefreshData]);
  // Prevent API call from being made except at first run and when triggered by user action.

  async function onAddTool() {
    props.history.push({
      pathname: ROUTE_NAME.TOOL_EDIT,
      state: {
        "editType": EDIT_TYPE.NEW
      }
    });
  }

  async function onEditTool(toolData) {
    props.history.push({
      pathname: ROUTE_NAME.TOOL_EDIT,
      state: {
        "editType": EDIT_TYPE.UPDATE,
        "toolData": toolData
      }
    });
  }

  return (
    dataState.error ?
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
            An error occurred accessing the API. Please try again later.
        </Alert>
      : (!dataState.isLoaded) ?
        <CircularProgress data-testid='circular-progress' />
        :
        <Container>
          <div style={{ width: '100%' }}>
            <Box display="flex" mb={2}>
              <Box flexGrow={1} >
                <Typography variant="h5">Tool Management</Typography>
              </Box>
              <Box>
                <Button align="justify-right" variant="contained" color="primary" onClick={() => onAddTool()} >Add Tool</Button>
              </Box>
            </Box>
          </div>

          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell align="center">Name</TableCell>
                  <TableCell align="center">Type</TableCell>
                  <TableCell align="center">Restriction</TableCell>
                  <TableCell align="center">Enabled</TableCell>
                  <TableCell align="center"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataState.items.map((item) => (
                  <TableRow key={item.id.N}>
                    <TableCell component="th" scope="row">
                      {item.id.N}
                    </TableCell>
                    <TableCell align="center">{item.name ? item.name.S : ""}</TableCell>
                    <TableCell align="center">{item.type ? item.type.S : ""}</TableCell>
                    <TableCell align="center">{item.restriction ? item.restriction.S : ""}</TableCell>
                    <TableCell align="center">
                      <Checkbox disabled checked={item.enabled ? item.enabled.BOOL : false} name="enabled" />
                    </TableCell>
                    <TableCell align="center"><IconButton onClick={() => onEditTool(item)}><EditIcon /></IconButton></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
  );
}