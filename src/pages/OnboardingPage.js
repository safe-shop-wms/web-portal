import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField, Grid, Paper, Typography, } from "@material-ui/core";
import { Auth } from "aws-amplify";
import ROUTE_NAME from '../navigation/CONSTANTS';

// Onboarding page to collect user's name and permanent password upon first login.

// Page styling
const useStyles = makeStyles((theme) => ({
  background: {
    padding: theme.spacing(5)
  }
}));

export default function OnboardingPage({props, setIsAuthenticated, userData}) {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleSubmit = async event => {
    event.preventDefault();
    if(password === confirmPassword) {
        try {
          await Auth.completeNewPassword(userData, password, { name: name });
          setIsAuthenticated(true);
          props.history.push(ROUTE_NAME.HOME);
      } catch (e) {
          alert(e.message);
      }
    } else {
      alert('Passwords do not match.');
    }
}

  return (
    <>
      <Grid container spacing={0} justify="center" direction="row">
        <Grid item>
          <Grid container direction="column" justify="center" spacing={2} >
            <Paper variant="elevation" elevation={2} className={classes.background}>
              <Grid item>
                <Typography component="h1" variant="h5">Complete Onboarding</Typography>
              </Grid>
              <Grid item>
                <form onSubmit={handleSubmit}>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <TextField type="text" placeholder="Name" fullWidth name="name" variant="outlined"
                        value={name} onChange={(event) => setName(event.target.value)}
                        required autoFocus />
                    </Grid>
                    <Grid item>
                      <TextField type="password" placeholder="New Password" fullWidth name="password" variant="outlined"
                        value={password} onChange={(event) => setPassword(event.target.value)}
                        required />
                    </Grid>
                    <Grid item>
                      <TextField type="password" placeholder="Confirm Password" fullWidth name="confirmPassword" variant="outlined"
                        value={confirmPassword} onChange={(event) => setConfirmPassword(event.target.value)}
                        required />
                    </Grid>
                    <Grid item>
                      <Button variant="contained" color="primary" type="submit" >
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}