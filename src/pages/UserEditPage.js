import React, { useState } from 'react'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { addUser, updateUser, USER_MAX_PIN_LENGTH } from '../utils/UserApi';
import Grid from '@material-ui/core/Grid';
import ROUTE_NAME from '../navigation/CONSTANTS';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { TableContainer } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { EDIT_TYPE } from '../utils/PageTypeConstants';

// Edit page to add or update a user.

// Page styling
const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function UserEditPage({ props }) {
  const classes = useStyles();
  const newUser = props.location.state.editType === EDIT_TYPE.NEW ? true : false;
  const user = props.location.state.userData;
  const tools = props.location.state.toolData;
  const trainings = props.location.state.trainingData;

  // Set initial values using existing user data if not a new user.
  const [id, setId] = useState(newUser ? "" : user.id.N);
  const [firstName, setFirstName] = useState(newUser || !user.first_name ? "" : user.first_name.S);
  const [lastName, setLastName] = useState(newUser || !user.last_name ? "" : user.last_name.S);
  const [cardId, setCardId] = useState(newUser || !user.card_id ? "" : user.card_id.S);
  const [pin, setPin] = useState(newUser || !user.pin ? "" : user.pin.S);
  const [authorizedTools, setAuthorizedTools] = useState(setInitialCheckboxStateForToolInfo("authorized_tools"))
  const [completedToolQuizzes, setCompletedToolQuizzes] = useState(setInitialCheckboxStateForToolInfo("completed_tool_quizzes"));
  const [completedToolTraining, setCompletedToolTraining] = useState(setInitialCheckboxStateForToolInfo("completed_tool_training"));
  const [completedGeneralTraining, setCompletedGeneralTraining] = useState(setInitialCheckboxStateForGeneralTraining());

  // Used to trigger state updates when checkboxes updated.
  const [triggerState, setTriggerState] = useState(true);

  // Track whether updatable values were updated
  const [firstNameUpdated, setFirstNameUpdated] = useState(false);
  const [lastNameUpdated, setLastNameUpdated] = useState(false);
  const [pinUpdated, setPinUpdated] = useState(false);
  const [authorizedToolsUpdated, setAuthorizedToolsUpdated] = useState(false);
  const [completedToolQuizzesUpdated, setCompletedToolQuizzesUpdated] = useState(false);
  const [completedToolTrainingUpdated, setCompletedToolTrainingUpdated] = useState(false);
  const [completedGeneralTrainingUpdated, setCompletedGeneralTrainingUpdated] = useState(false);

  const [isSubmitting, setIsSubmitting] = useState(false);

  // If new user, set all info as false.
  // Else, use user data to set info statuses.
  function setInitialCheckboxStateForToolInfo(toolInfoKey) {
    let checkboxes = {};
    if (newUser || !user[toolInfoKey]) {
      for (var tool of tools) {
        checkboxes[tool.id.N] = false;
      }
    } else {
      for (tool of tools) {
        checkboxes[tool.id.N] = user[toolInfoKey].NS.includes(tool.id.N);
      }
    }
    return checkboxes;
  }

  // If new user, set all general training as false.
  // Else, use user data to set training statuses.
  function setInitialCheckboxStateForGeneralTraining() {
    let checkboxes = {};
    let trainingInfoKey = "completed_general_training";
    if (newUser || !user[trainingInfoKey]) {
      for (var training of trainings) {
        checkboxes[training.id.N] = false;
      }
    } else {
      for (training of trainings) {
        checkboxes[training.id.N] = user[trainingInfoKey].NS.includes(training.id.N);
      }
    }
    return checkboxes;
  }

  // From dictionary of checked items, create array of items that were true. 
  function getArrayOfCheckedItems(checkedItemState) {
    let checkedItems = [];
    for (var itemId in checkedItemState) {
      if (checkedItemState[itemId]) {
        checkedItems.push(itemId)
      }
    }
    return checkedItems;
  }

  const handleChange = (event) => {
    switch (event.target.id) {
      case "id":
        setId(event.target.value);
        break;
      case "firstName":
        setFirstName(event.target.value);
        if (!firstNameUpdated) setFirstNameUpdated(true);
        break;
      case "lastName":
        setLastName(event.target.value);
        if (!lastNameUpdated) setLastNameUpdated(true);
        break;
      case "cardId":
        setCardId(event.target.value);
        break;
      case "pin":
        setPin(event.target.value);
        if (!pinUpdated) setPinUpdated(true);
        break;
      default:
      // Unreachable. Update failed.  
    }
  };

  const handleToolInfoChange = (event) => {
    let eventIdInfo = event.target.id.split('-');
    let updateType = eventIdInfo[0];
    let toolId = eventIdInfo[1];

    if (updateType === 'quiz') {

      let updatedCompletedToolQuizzes = completedToolQuizzes;
      updatedCompletedToolQuizzes[toolId] = event.target.checked;
      setCompletedToolQuizzes(updatedCompletedToolQuizzes);
      if (!completedToolQuizzesUpdated) setCompletedToolQuizzesUpdated(true);

    } else if (updateType === 'training') {

      let updatedCompletedToolTraining = completedToolTraining;
      updatedCompletedToolTraining[toolId] = event.target.checked;
      setCompletedToolTraining(updatedCompletedToolTraining);
      if (!completedToolTrainingUpdated) setCompletedToolTrainingUpdated(true);

    } else if (updateType === 'authorized') {

      let updatedAuthorizedTools = authorizedTools;
      updatedAuthorizedTools[toolId] = event.target.checked;
      setAuthorizedTools(updatedAuthorizedTools);
      if (!authorizedToolsUpdated) setAuthorizedToolsUpdated(true);

    }
    //Need to trigger state update so checkboxes update
    setTriggerState(!triggerState);
  };

  const handleGeneralTrainingChange = (event) => {
    let trainingId = event.target.id;
    let updatedCompletedGeneralTraining = completedGeneralTraining;
    updatedCompletedGeneralTraining[trainingId] = event.target.checked;
    setCompletedGeneralTraining(updatedCompletedGeneralTraining);
    if (!completedGeneralTrainingUpdated) setCompletedGeneralTrainingUpdated(true);

    //Need to trigger state update so checkboxes update
    setTriggerState(!triggerState);
  };

  async function onCancel() {
    props.history.replace(ROUTE_NAME.USERS);
  }

  async function onSubmit() {
    setIsSubmitting(true);
    try {
      if (newUser) {
        if (id === "" || firstName === "" || lastName === "" || pin === "") {
          setIsSubmitting(false);
          alert("ID, First Name, Last Name, and PIN are all required fields. Complete all fields before submitting.");
        } else if (pin.length > USER_MAX_PIN_LENGTH) {
          setIsSubmitting(false);
          alert(`PIN max length is ${USER_MAX_PIN_LENGTH}.`);
        } else {
          await addUser(id, firstName, lastName, pin,
            getArrayOfCheckedItems(authorizedTools),
            getArrayOfCheckedItems(completedToolQuizzes),
            getArrayOfCheckedItems(completedToolTraining),
            getArrayOfCheckedItems(completedGeneralTraining));
          props.history.replace(ROUTE_NAME.USERS);
        }
      } else {
        if (!firstNameUpdated && !lastNameUpdated && !pinUpdated && !authorizedToolsUpdated
          && !completedToolQuizzesUpdated && !completedToolTrainingUpdated && !completedGeneralTrainingUpdated) {
          setIsSubmitting(false);
          alert("No values updated.");
        } else if (pin.length > USER_MAX_PIN_LENGTH) {
          setIsSubmitting(false);
          alert(`PIN max length is ${USER_MAX_PIN_LENGTH}.`);
        } else {
          let updatedInfo = {};
          if (firstNameUpdated) {
            updatedInfo["firstName"] = firstName;
          }
          if (lastNameUpdated) {
            updatedInfo["lastName"] = lastName;
          }
          if (pinUpdated) {
            updatedInfo["pin"] = pin;
          }
          if (authorizedToolsUpdated) {
            updatedInfo["authorizedTools"] = getArrayOfCheckedItems(authorizedTools);
          }
          if (completedToolQuizzesUpdated) {
            updatedInfo["completedToolQuizzes"] = getArrayOfCheckedItems(completedToolQuizzes);
          }
          if (completedToolTrainingUpdated) {
            updatedInfo["completedToolTraining"] = getArrayOfCheckedItems(completedToolTraining);
          }
          if (completedGeneralTrainingUpdated) {
            updatedInfo["completedGeneralTraining"] = getArrayOfCheckedItems(completedGeneralTraining);
          }

          await updateUser(id, updatedInfo);
          props.history.replace(ROUTE_NAME.USERS);
        }
      }
    } catch (error) {
      alert(`Error occured when submitting the data. ${error}`);
    }
  }

  return (
    <Container>
      <div style={{ width: '100%' }}>
        <Box display="flex" mb={2}>
          <Box flexGrow={1} >
            <Typography variant="h5">{newUser ? "Add User" : "Edit User"}</Typography>
          </Box>
          <Box>
            <Button disabled={isSubmitting} variant="contained" color="primary" onClick={() => onSubmit()} >Submit</Button>
          </Box>
          <Box ml={2}>
            <Button disabled={isSubmitting} variant="contained" onClick={() => onCancel()} >Cancel</Button>
          </Box>
        </Box>
      </div>

      <form className={classes.root} autoComplete="off">
        <div>
          <TextField
            required
            id="id"
            label="ID"
            type="number"
            InputProps={{
              readOnly: newUser ? false : true,
            }}
            variant="outlined"
            value={id}
            onChange={handleChange}
          />
          <TextField
            required
            id="firstName"
            label="First Name"
            variant="outlined"
            value={firstName}
            onChange={handleChange}
          />
          <TextField
            required
            id="lastName"
            label="Last Name"
            variant="outlined"
            value={lastName}
            onChange={handleChange}
          />
          <TextField
            id="cardId"
            label="Card ID"
            InputProps={{
              readOnly: true,
            }}
            variant="outlined"
            value={cardId}
          />
          <TextField
            required
            id="pin"
            label={`PIN (Max ${USER_MAX_PIN_LENGTH} digits)`}
            type="number"
            variant="outlined"
            value={pin}
            onChange={handleChange}
          />
        </div>
      </form>

      <Grid container>
        <Grid item xs={6}>
          <Box mb={2} mt={2}>
            <Typography variant="h6">Tool Training & Authorization</Typography>
          </Box>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="center">Safety Quiz</TableCell>
                  <TableCell align="center">Hands On Training</TableCell>
                  <TableCell align="center">Authorized</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tools.map((tool) => (
                  <TableRow key={tool.id.N}>
                    <TableCell component="th" scope="row">{tool.name.S}</TableCell>
                    <TableCell align="center">
                      <Checkbox
                        id={`quiz-${tool.id.N}`}
                        checked={completedToolQuizzes[tool.id.N]}
                        onChange={handleToolInfoChange}
                        color="primary"
                      />
                    </TableCell>
                    <TableCell align="center">
                      <Checkbox
                        id={`training-${tool.id.N}`}
                        checked={completedToolTraining[tool.id.N]}
                        onChange={handleToolInfoChange}
                        color="primary"
                      />
                    </TableCell>
                    <TableCell align="center">
                      <Checkbox
                        id={`authorized-${tool.id.N}`}
                        checked={authorizedTools[tool.id.N]}
                        disabled={!completedToolQuizzes[tool.id.N] || !completedToolTraining[tool.id.N]}
                        onChange={handleToolInfoChange}
                        color="primary"
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <Grid item xs={4}>
          <Box mb={2} mt={2} ml={4}>
            <Typography variant="h6">General Training</Typography>
          </Box>
          <Box ml={4}>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell align="center">Completed</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {trainings.map((training) => (
                    <TableRow key={training.id.N}>
                      <TableCell component="th" scope="row">{training.name.S}</TableCell>
                      <TableCell align="center">
                        <Checkbox
                          id={training.id.N}
                          checked={completedGeneralTraining[training.id.N]}
                          onChange={handleGeneralTrainingChange}
                          color="primary"
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}