import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField, Grid, Paper, Typography, } from "@material-ui/core";
import { Auth } from "aws-amplify";
import ROUTE_NAME from '../navigation/CONSTANTS';

// Login page using username and password.
// After login, routes to onboarding if user is new, otherwise routes to home.

// Page styling
const useStyles = makeStyles((theme) => ({
  background: {
    padding: theme.spacing(5)
  }
}));

export default function LoginPage({ props, setIsAuthenticated, setUserData }) {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const user = await Auth.signIn(username, password);
      if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
        setUserData(user);
        props.history.push(ROUTE_NAME.ONBOARDING);
      } else {
        setIsAuthenticated(true);
        props.history.push(ROUTE_NAME.HOME);
      }
    } catch (e) {
      alert(e.message);
    }
  }

  return (
    <>
      <Grid container spacing={0} justify="center" direction="row">
        <Grid item>
          <Grid container direction="column" justify="center" spacing={2} >
            <Paper variant="elevation" elevation={2} className={classes.background}>
              <Grid item>
                <Typography component="h1" variant="h5">Log In</Typography>
              </Grid>
              <Grid item>
                <form onSubmit={handleSubmit}>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <TextField type="text" placeholder="Username" fullWidth name="username" variant="outlined"
                        value={username} onChange={(event) => setUsername(event.target.value)}
                        required autoFocus />
                    </Grid>
                    <Grid item>
                      <TextField type="password" placeholder="Password" fullWidth name="password" variant="outlined"
                        value={password} onChange={(event) => setPassword(event.target.value)}
                        required />
                    </Grid>
                    <Grid item>
                      <Button variant="contained" color="primary" type="submit" >
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}