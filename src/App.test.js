import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

// Needs update
test('App renders app bar', () => {
  render(<App />);

  const appBar = screen.getByText(/safe shop management system/i);
  expect(appBar).toBeInTheDocument();
});