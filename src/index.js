import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import App from './App';
import theme from './theme';
import 'fontsource-roboto';
import config from "./config";
import Amplify from "aws-amplify";

const configData = config();

// Configure backend service connections
Amplify.configure({
   Auth: {
       mandatorySignIn: true,
       region: configData.cognito.REGION,
       userPoolId: configData.cognito.USER_POOL_ID,
       identityPoolId: configData.cognito.IDENTITY_POOL_ID,
       userPoolWebClientId: configData.cognito.APP_CLIENT_ID
   },
   API: {
    endpoints: [
        {
            name: configData.apiGateway.NAME,
            endpoint: configData.apiGateway.URL,
            region: configData.apiGateway.REGION
        },
    ]
}
});

ReactDOM.render(
  <React.StrictMode>
  <ThemeProvider theme={theme}>
  <CssBaseline/>
    <Router>
    <App />
    </Router>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
