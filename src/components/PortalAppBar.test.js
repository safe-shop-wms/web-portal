import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../App';

// Needs update
test('App renders icon and title', () => {
  render(<App />);

  const buildIcon = screen.getByTestId('build-icon');
  expect(buildIcon).toBeInTheDocument();
  const appBar = screen.getByText(/safe shop management system/i);
  expect(appBar).toBeInTheDocument();
});