import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import BuildIcon from '@material-ui/icons/Build';
import GroupIcon from '@material-ui/icons/Group';
import SchoolIcon from '@material-ui/icons/School';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import ROUTE_NAME from '../navigation/CONSTANTS';

// Drawer Menu for Navigation

// Styling values
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  drawer: {
    display: 'flex',
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  drawerPaper: {
    width: drawerWidth,
  },
}));

// List item to support navigation
function ListItemLink(props) {
  const { icon, label, to } = props;

  const renderLink = React.useMemo(
    () => React.forwardRef((itemProps, ref) => <RouterLink to={to} ref={ref} {...itemProps} />),
    [to],
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    </li>
  );
}

ListItemLink.propTypes = {
  icon: PropTypes.element,
  label: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default function DrawerMenu() {
  
  const classes = useStyles;
  return (
    <Drawer variant="permanent" className={classes.drawer} classes={{ paper: classes.drawerPaper }}>
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>
          <ListItemLink to={ROUTE_NAME.USERS} label="Users" icon={<GroupIcon />} />
          <ListItemLink to={ROUTE_NAME.TOOLS} label="Tools" icon={<BuildIcon />} />
          <ListItemLink to={ROUTE_NAME.TRAINING} label="Training" icon={<SchoolIcon />} />
        </List>
      </div>
    </Drawer>
  )
}