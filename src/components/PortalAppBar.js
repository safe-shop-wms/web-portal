import React, { useState } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import BuildIcon from '@material-ui/icons/Build';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

// App Bar with a profile section and menu for authenticated users

// App bar styling
const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));

// Styled profile menu
const ProfileMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    PaperProps={{
          style: {
            width: '20ch',
          },
        }}
    {...props}
  />
));

// Styled profile section button
const ProfileButton = withStyles((theme) => ({
  root: {
    color: theme.palette.primary.contrastText,
  },
}))(Button);

// The App Bar itself
export default function PortalAppBar(props) {
  const classes = useStyles();

  const [profileMenuAnchor, setProfileMenuAnchor] = useState(null);

  const handleProfileClick = (event) => {
    setProfileMenuAnchor(event.currentTarget);
  };

  const handleProfileClose = () => {
    setProfileMenuAnchor(null);
  };

  const handleLogOut = () => {
    handleProfileClose();
    props.handleLogOut();
  }

  return (
    <AppBar color="primary" position="fixed" className={classes.appBar}>
      <Toolbar>
        <div style={{ width: '100%' }}>
          <Box display="flex" alignItems="center" pt={1} pb={1} >
            <Box marginRight={2}>
              <BuildIcon data-testid="build-icon"></BuildIcon>
            </Box>
            <Box flexGrow={1} >
              <Typography variant="h5" >
                Safe Shop Management System
              </Typography>
            </Box>
            {props.isAuthenticated ?
              <Box>
                <ProfileButton onClick={handleProfileClick}>
                  <AccountCircleIcon/>
                  <Box ml={2} mr={1} >
                    <Typography >{props.username}</Typography>
                  </Box>
                  <ExpandMoreIcon />
                </ProfileButton>
                <ProfileMenu
                  anchorEl={profileMenuAnchor}
                  keepMounted
                  open={Boolean(profileMenuAnchor)}
                  onClose={handleProfileClose}
                >
                  <MenuItem onClick={handleLogOut}>Log Out</MenuItem>
                </ProfileMenu>
              </Box>
              : null
            }
          </Box>
        </div>
      </Toolbar>
    </AppBar>
  )
}