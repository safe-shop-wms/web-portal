import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import PropTypes from 'prop-types';

// Dialog prompts user to confirm or cancel an action for a particular item.

function ConfirmActionDialog(props) {
    const { actionDescription, itemId, onConfirm, onCancel, open } = props;

    return (
        <Dialog onClose={onCancel} open={open}>
            <DialogTitle>{actionDescription} {itemId}?</DialogTitle>
            <DialogActions>
                <Button onClick={onCancel} color="primary">
                    Cancel
            </Button>
                <Button onClick={onConfirm} color="primary" autoFocus>
                    Yes
            </Button>
            </DialogActions>
        </Dialog>
    );
}

ConfirmActionDialog.propTypes = {
    actionDescription: PropTypes.string.isRequired,
    itemId: PropTypes.string.isRequired,
    onConfirm: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};

export default ConfirmActionDialog;