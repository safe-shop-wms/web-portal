import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Routes from './navigation/Routes.js';
import PortalAppBar from './components/PortalAppBar.js';
import DrawerMenu from './components/DrawerMenu.js';
import Toolbar from '@material-ui/core/Toolbar';
import { Auth } from "aws-amplify";
import { withRouter } from 'react-router';
import ROUTE_NAME from './navigation/CONSTANTS';

// The single-page app

// App page styling
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  loginContent: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginLeft: 150,
  },
}));

function App(props) {
  const classes = useStyles();
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [userData, setUserData] = useState();
  const [username, setUsername] = useState("");

  async function handleLogOut() {
    await Auth.signOut();
    setIsAuthenticated(false);
    props.history.push(ROUTE_NAME.HOME);
  }

  //Runs on first mount and when `isAuthenticated` changes
  useEffect(() => {
    async function getAuth() {
      try {
        await Auth.currentSession();
        const userInfo = await Auth.currentUserInfo();
        setIsAuthenticated(true);
        setUsername(userInfo.username);
        props.history.push(ROUTE_NAME.HOME);
      }
      catch(e) {
        if (e !== 'No current user') {
          alert(e);
        }
      }
    }

    getAuth();    
  }, [isAuthenticated, props.history]);

  return (
    <div className={classes.root}>
      <PortalAppBar isAuthenticated={isAuthenticated} username={username} handleLogOut={handleLogOut}/>
      { isAuthenticated ? 
        <DrawerMenu/>
        : null
      }    
      <main className={isAuthenticated ? classes.content : classes.loginContent}>
      <Toolbar />
      <Routes props={props} setIsAuthenticated={setIsAuthenticated} isAuthenticated={isAuthenticated} setUserData={setUserData} userData={userData}/>
      </main>
    </div>
  );
}

export default withRouter(App);