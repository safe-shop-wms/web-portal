import { createMuiTheme } from '@material-ui/core/styles';

// Customize web portal color scheme
const PRIMARY_COLOR = '#740001';
const SECONDARY_COLOR = '#D3A625';
const ERROR_COLOR = '#c62828';
const BACKGROUND_COLOR = '#fff';

// Color scheme applied
const theme = createMuiTheme({
  palette: {
    primary: {
      main: PRIMARY_COLOR,
    },
    secondary: {
      main: SECONDARY_COLOR,
    },
    error: {
      main: ERROR_COLOR,
    },
    background: {
      default: BACKGROUND_COLOR,
    },
  },
});

export default theme;