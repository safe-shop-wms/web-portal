// Utility functions for dealing with data

// Returns a function which compares two items by their IDs (item.id.N).
// That function returns 1 if item1 is greater, -1 if item2 is less, 0 if they are equal.
function sortById() {
  return function (item1, item2) {
    if (parseInt(item1.id.N) > parseInt(item2.id.N)) {
      return 1;
    } else if (parseInt(item1.id.N) < parseInt(item2.id.N)) {
      return -1;
    }
    return 0;
  }
}

export { sortById };