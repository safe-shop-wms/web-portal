import {API} from 'aws-amplify';
import {API_NAME, getInitData} from './ApiHelper';

// Functions for working with the users table and API

const USERS_PATH = '/users';
const USERS_TABLE_NAME = process.env.REACT_APP_AWS_USERS_TABLE; 
const USER_MAX_PIN_LENGTH = 8;

async function getUsers() {
    let initData = await getInitData();
    return API.get(API_NAME, USERS_PATH, initData);
}

async function addUser(id, firstName, lastName, pin, authorizedTools,
    completedToolQuizzes, completedToolTraining, completedGeneralTraining) {
    let requestBody = {
        "Item": {
            "id": {
                "N": id
            },
            "first_name": {
                "S": firstName
            },
            "last_name": {
                "S": lastName
            },
            "pin": {
                "S": pin
            }
        },
        "TableName": USERS_TABLE_NAME
    }
    if(authorizedTools.length !== 0) {
        requestBody["Item"]["authorized_tools"] = {
            "NS": authorizedTools
        }
    }
    if(completedToolQuizzes.length !== 0) {
        requestBody["Item"]["completed_tool_quizzes"] = {
            "NS": completedToolQuizzes
        }
    }
    if(completedToolTraining.length !== 0) {
        requestBody["Item"]["completed_tool_training"] = {
            "NS": completedToolTraining
        }
    }
    if(completedGeneralTraining.length !== 0) {
        requestBody["Item"]["completed_general_training"] = {
            "NS": completedGeneralTraining
        }
    }

    let initData = await getInitData(requestBody);
    console.log(initData);
    return API.post(API_NAME, USERS_PATH, initData);
}

async function updateUser(id, updatedInfo) {
    let updateExpressionAttributes = [];
    let expressionAttributeValues = {};

    if("firstName" in updatedInfo) {
        updateExpressionAttributes.push("first_name=:firstName");
        expressionAttributeValues[":firstName"] = {
            "S": updatedInfo["firstName"]
        }
    }
    if("lastName" in updatedInfo) {
        updateExpressionAttributes.push("last_name=:lastName");
        expressionAttributeValues[":lastName"] = {
            "S": updatedInfo["lastName"]
        }
    }
    if("pin" in updatedInfo) {
        updateExpressionAttributes.push("pin=:pin");
        expressionAttributeValues[":pin"] = {
            "S": updatedInfo["pin"]
        }
    }
    if("authorizedTools" in updatedInfo) {
        updateExpressionAttributes.push("authorized_tools=:authorizedTools");
        expressionAttributeValues[":authorizedTools"] = {
            "NS": updatedInfo["authorizedTools"]
        }
    }
    if("completedToolQuizzes" in updatedInfo) {
        updateExpressionAttributes.push("completed_tool_quizzes=:completedToolQuizzes");
        expressionAttributeValues[":completedToolQuizzes"] = {
            "NS": updatedInfo["completedToolQuizzes"]
        }
    }
    if("completedToolTraining" in updatedInfo) {
        updateExpressionAttributes.push("completed_tool_training=:completedToolTraining");
        expressionAttributeValues[":completedToolTraining"] = {
            "NS": updatedInfo["completedToolTraining"]
        }
    }
    if("completedGeneralTraining" in updatedInfo) {
        updateExpressionAttributes.push("completed_general_training=:completedGeneralTraining");
        expressionAttributeValues[":completedGeneralTraining"] = {
            "NS": updatedInfo["completedGeneralTraining"]
        }
    }

    // Create updateExpression, adding commas in between attributes
    let updateExpression = "SET ";
    let updateAttributeCount = updateExpressionAttributes.length;
    for(var i = 0; i < updateAttributeCount; i++) {
        updateExpression += updateExpressionAttributes[i];
        if(i !== updateAttributeCount - 1) {
            updateExpression += ", ";
        }
    }

    let requestBody = {
        "UpdateExpression": updateExpression,
        "ExpressionAttributeValues": expressionAttributeValues,
        "Key": {
            "id": {
                "N": id
            }
        },
        "TableName": USERS_TABLE_NAME
    }
    let initData = await getInitData(requestBody);
    console.log(initData);
    return API.put(API_NAME, USERS_PATH, initData);
}

async function deleteUser(userId) {
    let path = `${USERS_PATH}/${userId}`
    let initData = await getInitData();
    return API.del(API_NAME, path, initData);
}

export { getUsers, addUser, updateUser, deleteUser, USER_MAX_PIN_LENGTH };