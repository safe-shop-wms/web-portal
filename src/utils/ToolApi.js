import {API} from 'aws-amplify';
import {API_NAME, getInitData} from './ApiHelper';

// Functions for working with the tools table and API

const TOOLS_PATH = '/tools';
const TOOLS_TABLE_NAME = process.env.REACT_APP_AWS_TOOLS_TABLE;
const TOOL_TYPE = {HANDHELD: "Handheld", STATIONARY: "Stationary"}
const TOOL_RESTRICTION = {UNRESTRICTED: "Unrestricted", FIFTEEN_PLUS: "15+", ADULT_ONLY: "Adult Only" }

async function getTools() {
    let initData = await getInitData();
    return API.get(API_NAME, TOOLS_PATH, initData);
}

async function addTool(id, name, type, restriction, enabled) {
    let requestBody = {
        "Item": {
            "id": {
                "N": id
            },
            "name": {
                "S": name
            },
            "type": {
                "S": type
            },
            "restriction": {
                "S": restriction
            },
            "enabled": {
                "BOOL": enabled
            }
        },
        "TableName": TOOLS_TABLE_NAME
    }
        
    let initData = await getInitData(requestBody);
    return API.post(API_NAME, TOOLS_PATH, initData);
}

async function updateTool(id, updatedInfo) {
    let updateExpressionAttributes = [];
    let expressionAttributeValues = {};
    let expressionAttributeNames = {};

    if("name" in updatedInfo) {
        updateExpressionAttributes.push("#toolName=:toolName");
        expressionAttributeNames["#toolName"] = "name";
        expressionAttributeValues[":toolName"] = {
            "S": updatedInfo["name"]
        }
    }
    if("type" in updatedInfo) {
        updateExpressionAttributes.push("#toolType=:toolType");
        expressionAttributeNames["#toolType"] = "type";
        expressionAttributeValues[":toolType"] = {
            "S": updatedInfo["type"]
        }
    }
    if("restriction" in updatedInfo) {
        updateExpressionAttributes.push("restriction=:restriction");
        expressionAttributeValues[":restriction"] = {
            "S": updatedInfo["restriction"]
        }
    }
    if("enabled" in updatedInfo) {
        updateExpressionAttributes.push("enabled=:enabled");
        expressionAttributeValues[":enabled"] = {
            "BOOL": updatedInfo["enabled"]
        }
    }

    // Create updateExpression, adding commas in between attributes
    let updateExpression = "SET ";
    let updateAttributeCount = updateExpressionAttributes.length;
    for(var i = 0; i < updateAttributeCount; i++) {
        updateExpression += updateExpressionAttributes[i];
        if(i !== updateAttributeCount - 1) {
            updateExpression += ", ";
        }
    }

    let requestBody = {
        "UpdateExpression": updateExpression,
        "ExpressionAttributeValues": expressionAttributeValues,
        "Key": {
            "id": {
                "N": id
            }
        },
        "TableName": TOOLS_TABLE_NAME
    }
    if(Object.keys(expressionAttributeNames).length > 0) {
        requestBody["ExpressionAttributeNames"] = expressionAttributeNames;
    }

    let initData = await getInitData(requestBody);
    return API.put(API_NAME, TOOLS_PATH, initData);
}

export { TOOL_TYPE, TOOL_RESTRICTION, getTools, addTool, updateTool };