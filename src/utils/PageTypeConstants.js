// Constants to define page type

const EDIT_TYPE = { NEW: "new", UPDATE: "update" };

export { EDIT_TYPE };