import {API} from 'aws-amplify';
import {API_NAME, getInitData} from './ApiHelper';

// Functions for working with the training table and API

const TRAINING_PATH = '/training';
const TRAINING_TABLE_NAME = process.env.REACT_APP_AWS_TRAINING_TABLE;

async function getTraining() {
    let initData = await getInitData();
    return API.get(API_NAME, TRAINING_PATH, initData);
}

async function addTraining(id, name) {
    let requestBody = {
        "Item": {
            "id": {
                "N": id
            },
            "name": {
                "S": name
            }
        },
        "TableName": TRAINING_TABLE_NAME
    }
        
    let initData = await getInitData(requestBody);
    return API.post(API_NAME, TRAINING_PATH, initData);
}

async function updateTraining(id, updatedInfo) {
    let updateExpressionAttributes = [];
    let expressionAttributeValues = {};
    let expressionAttributeNames = {};

    if("name" in updatedInfo) {
        updateExpressionAttributes.push("#trainingName=:trainingName");
        expressionAttributeNames["#trainingName"] = "name";
        expressionAttributeValues[":trainingName"] = {
            "S": updatedInfo["name"]
        }
    }

    // Create updateExpression, adding commas in between attributes
    let updateExpression = "SET ";
    let updateAttributeCount = updateExpressionAttributes.length;
    for(var i = 0; i < updateAttributeCount; i++) {
        updateExpression += updateExpressionAttributes[i];
        if(i !== updateAttributeCount - 1) {
            updateExpression += ", ";
        }
    }

    let requestBody = {
        "UpdateExpression": updateExpression,
        "ExpressionAttributeValues": expressionAttributeValues,
        "Key": {
            "id": {
                "N": id
            }
        },
        "TableName": TRAINING_TABLE_NAME
    }
    if(Object.keys(expressionAttributeNames).length > 0) {
        requestBody["ExpressionAttributeNames"] = expressionAttributeNames;
    }

    let initData = await getInitData(requestBody);
    return API.put(API_NAME, TRAINING_PATH, initData);
}

export { getTraining, addTraining, updateTraining };