import { Auth } from 'aws-amplify';

// Utility functions for API calls

const API_NAME = process.env.REACT_APP_AWS_API_NAME;

// Sets data for request using given request body, if present,
// and sets auth header.
async function getInitData(requestBody) {
    let sessionObject = await Auth.currentSession();
    let idToken = sessionObject.idToken.jwtToken;
    var initData;
    if (requestBody) {
        initData = {
            body: requestBody,
            headers: {
                Authorization: idToken
            },
            response: true
        }
    } else {
        initData = {
            headers: {
                Authorization: idToken
            },
            response: true
        }
    }
    return initData;
}

export { API_NAME, getInitData };