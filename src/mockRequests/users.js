// Mock API Responses for the 'users' API

// Needs update

// Make n users to use in response data
const makeUsers = (n) => {
  const users = [];
  for (let i = 0; i < n; i++) {
    users.push({
      "last_name": {
        "S": `Last_${i}`
      },
      "id": {
        "N": i
      },
      "authorized_tools": {
        "NS": [
          "1"
        ]
      },
      "card_id": {
        "S": `1234567890${i}`
      },
      "first_name": {
        "S": `First_${i}`
      }
    })
  }
  return users;
}

// Response from GET /users returns 10 users
const getUsers = () => {
  var userItems = makeUsers(10);
  return {
    data: {
      "Count": 10,
      "Items": userItems,
      "ScannedCount": 10
    },
    status: 200,
    statusText: 'OK',
    config: {},
    headers: {},
  }
}

export const getUsersResponse = getUsers();