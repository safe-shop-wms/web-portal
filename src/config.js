// Configuration for backend service connections
export default function config() {
    return ({
        apiGateway: {
            NAME: process.env.REACT_APP_AWS_API_NAME,
            REGION: process.env.REACT_APP_AWS_REGION,
            URL: process.env.REACT_APP_AWS_API_URL
        },
        cognito: {
            REGION: process.env.REACT_APP_AWS_REGION,
            USER_POOL_ID: process.env.REACT_APP_AWS_USER_POOL_ID,
            APP_CLIENT_ID: process.env.REACT_APP_AWS_APP_CLIENT_ID,
            IDENTITY_POOL_ID: process.env.REACT_APP_AWS_IDENTITY_POOL_ID
        }
       });
}